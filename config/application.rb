require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Timer
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.action_view.field_error_proc = proc { |html_tag, instance|
      if /\<label/.match?(html_tag)
        html_tag
      else
        errors = Array(instance.error_message).join(", ")
        %(#{html_tag}<div class="text-red-500 italic">#{errors}</div>).html_safe
      end
    }
  end
end
