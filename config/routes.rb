Rails.application.routes.draw do
  devise_for :users, controllers: {
               sessions: 'session/sessions'
             }

  resources :timers
  resources :entries, only: [:create, :update, :destroy] do
    post :stop_timer, on: :member
  end

  resource :profile, only: [:show, :update]

  namespace :admin do
    resources :reports
  end

  root to: 'timers#index'
end
