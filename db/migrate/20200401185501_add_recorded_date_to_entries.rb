class AddRecordedDateToEntries < ActiveRecord::Migration[6.0]
  def change
    add_column :entries, :recorded_date, :date
  end
end
