class CreateEntries < ActiveRecord::Migration[6.0]
  def change
    create_table :entries do |t|
      t.integer :duration
      t.datetime :start_time
      t.string :notes
      t.references :project, null: false

      t.timestamps
    end
  end
end
