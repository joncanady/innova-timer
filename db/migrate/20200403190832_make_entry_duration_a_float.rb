class MakeEntryDurationAFloat < ActiveRecord::Migration[6.0]
  def up
    change_column :entries, :duration, :float 
  end

  def down
    change_column :entries, :duration, :integer 
  end
end
