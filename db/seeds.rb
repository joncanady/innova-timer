if Rails.env.development?
  puts "Seeing database in dev mode:"
  puts "  => Deleting clients..."
  Client.delete_all
  puts "  => Deleting projects..."
  Project.delete_all
  puts "  => Deleting time entries..."
  Entry.delete_all
  puts "  => Deleting users..." 
  User.delete_all

  puts "  => Creating user..."
  User.create email: 'example@example.com', password: 'devlogin', password_confirmation: 'devlogin', name: "Development User"
  puts "  => Creating admin user..."
  User.create email: 'admin@example.com', password: 'devlogin', password_confirmation: 'devlogin', admin: true, name: "Development Admin"

  puts "  => Creating client 'Innova Parnters'..."
  client = Client.create name: 'Innova Partners'
  puts "  => Creating client 'Innova Parnters' project 'DrugSearch'..."
  client.projects.create name: 'DrugSearch'
end
