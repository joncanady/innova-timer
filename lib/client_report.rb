require "csv"

class ClientReport
  # Initialize a new ClientReport
  #
  # @param client_id [Integer] The ID of the client to report format
  # @param start_date [String] The start date in YYYY-MM-DD format
  # @param end_date [String] The end date in YYYY-MM-DD format
  def initialize(client_id:, start_date:, end_date:)
    @client_id  = client_id
    @start_date = start_date
    @end_date   = end_date
  end

  # Generates a report for the given client between the given date ranges.
  #
  # Results are in the format:
  # => {"Client Name": {"Project 1": [{entry, entry}], "Project 2": [...] ...}
  #
  # @return [Hash] All time entries for the given date range keyed by Client and Project
  def generate
    results = {}
    results[client.name] = {}

    entries = client.time_entries_between(@start_date, @end_date)
    entries.group_by(&:project_name).each_pair do |project_name, entries| 
      results[client.name][project_name] = entries
    end

    results
  end

  # Generate a CSV that can be read by Excel.
  #
  # Great for letting clients download "Excel" files.
  #
  # @reuturn [String] The CSV contents.
  def csv
    headers = %w(project employee date duration notes)
    entries = client.time_entries_between(@start_date, @end_date)

    CSV.generate do |csv| 
      csv << headers
      entries.each do |entry|
        csv << [entry.project_name,
                entry.user_name,
                entry.recorded_date.strftime("%Y-%M-%d"),
                entry.duration,
                entry.notes]
      end
    end
  end

  def client
    @client ||= Client.find(@client_id)
  end
end
