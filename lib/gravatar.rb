class Gravatar
  # Initialize a new Gravatar URL maker.
  #
  # @param email [String] The email address associated with the Gravatar account.
  def initialize(email)
    @email = email
  end

  # The URL to the gravatar for our email address.
  #
  # @return [String] The gravatar URL
  def image_url
    "https://www.gravatar.com/avatar/#{email_hash}"
  end

  def email_hash
    Digest::MD5.hexdigest(@email.strip.downcase)
  end
end

