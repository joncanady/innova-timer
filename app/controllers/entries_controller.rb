class EntriesController < ApplicationController
  before_action :set_date

  def create
    @new_entry = current_user.entries.new(entry_params)
    if @new_entry.save
      redirect_to timers_path(date: @new_entry.recorded_date.strftime("%Y-%m-%d"))
    else
      # load up the clients for the new entry form to rerender
      @clients = Client.ordered.all
    end
  end

  def update
  end

  def stop_timer
    entry = current_user.entries.find(params[:id])
    entry.stop_timer!
    redirect_to timers_path(date: entry.recorded_date.strftime("%Y-%m-%d"))
  end

  def destroy
  end

  private

  def entry_params
    params.require(:entry).permit(:duration, :notes, :project_id, :recorded_date)
  end
end
