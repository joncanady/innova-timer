class TimersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_date
  before_action :set_header_and_page

  def index
    @new_entry = current_user.entries.new
    @entries   = current_user.entries.for_date(@date)
    @total_duration = @entries.map(&:duration).compact.sum
    @clients   = Client.ordered.all
  end

  def set_header_and_page
    @header_text = @date.strftime("%B %-d, %Y")
    @page        = :timers
  end
end
