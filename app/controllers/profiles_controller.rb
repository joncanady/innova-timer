class ProfilesController < ApplicationController
  before_action :set_header_and_page

  def show
    @user = current_user
  end

  def update
    @user = current_user
    if @user.update(profile_params)
      redirect_to profile_path, notice: "Profile has been saved!"
    else
      render :show, error: "We couldn't save your profile. Sorry."
    end
  end

  private

  def profile_params
    params.require(:user).permit(:name)
  end

  def set_header_and_page
    @header_text = "Your Profile"
    @page        = :profile
  end
end
