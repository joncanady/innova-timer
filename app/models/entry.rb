class Entry < ApplicationRecord
  belongs_to :project
  belongs_to :user

  scope :for_date, ->(date) { where(recorded_date: date) }

  delegate :name, to: :project, prefix: true
  delegate :client_name, to: :project

  validates :project_id, presence: true
  validates :duration, numericality: true, allow_nil: true

  before_create :start_timer_if_no_duration

  def user_name
    user.display_name
  end

  def company_and_project_name
    "#{client_name} / #{project_name}"
  end

  def running_timer?
    duration.nil? && start_time.present?
  end

  def stop_timer!
    self[:duration] = ((Time.zone.now - start_time) / 60 / 60).round(2)
    save!
  end

  private

  def start_timer_if_no_duration
    return if duration.present? || start_time.present?
    self[:start_time] = Time.now
  end
end
