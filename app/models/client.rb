class Client < ApplicationRecord
  has_many :projects

  scope :ordered, -> {order(name: :asc)}

  def time_entries_between(start_date, end_date)
    Entry.where(project_id: projects.pluck(:id),
                recorded_date: (start_date..end_date))
  end
end
