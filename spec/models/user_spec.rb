require "rails_helper"

RSpec.describe User do
  context "Display names" do
    it "shows the user's name, if set" do
      user = FactoryBot.create(:user, name: "User's Name")
      expect(user.display_name).to eq(user.name)
    end

    it "shows the user's email if name isn't set" do
      user = FactoryBot.create(:user, name: nil)
      expect(user.display_name).to eq(user.email)
    end
  end
end
