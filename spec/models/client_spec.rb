require "rails_helper"

RSpec.describe Client do
  let(:client)      { FactoryBot.create :client }
  let(:project)     { FactoryBot.create :project, client: client }
  let(:num_entries) { 10 }

  it "can fetch its time entries between a given set of dates" do
    num_entries.times { |n| FactoryBot.create :entry, project: project, recorded_date: '2020-04-15' }
    entries = client.time_entries_between('2020-04-01', '2020-04-30')
    expect(entries.count).to eq(num_entries)
  end
end
