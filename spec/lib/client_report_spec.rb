require "rails_helper"
require "client_report"

RSpec.describe ClientReport do
  let!(:user1)   { FactoryBot.create :user }
  let!(:user2)   { FactoryBot.create :user }
  let!(:client) { FactoryBot.create :client }
  let!(:project1) { FactoryBot.create :project, client: client, name: "Project 1" }
  let!(:project2) { FactoryBot.create :project, client: client, name: "Project 2" }
  let!(:entryp1n1) { FactoryBot.create :entry, project: project1, user: user1, duration: "3.5", recorded_date: '2020-04-02' }
  let!(:entryp1n2) { FactoryBot.create :entry, project: project1, user: user2, duration: "8", recorded_date: '2020-04-02' }
  let!(:entryp1n3) { FactoryBot.create :entry, project: project1, user: user1, duration: "4.5", recorded_date: '2020-04-03' }
  let!(:entryp2n1) { FactoryBot.create :entry, project: project2, user: user2, duration: "5", recorded_date: '2020-04-05' }
  let!(:start_date) { '2020-04-01' }
  let!(:end_date) { '2020-04-30' }

  context "Generating report data" do
    subject { ClientReport.new(client_id: client.id,
                               start_date: start_date,
                               end_date: end_date).generate }

    it "keys off of client name" do
      expect(subject).to have_key(client.name)
    end

    it "has individual keys for each project name" do
      expect(subject[client.name]).to have_key(project1.name)
      expect(subject[client.name]).to have_key(project2.name)
    end

    it "breaks down time entries by project" do
      expect(subject[client.name][project1.name].count).to eq(3)
      expect(subject[client.name][project2.name].count).to eq(1)
    end
  end

  context "generating a CSV" do
    subject { ClientReport.new(client_id: client.id,
                               start_date: start_date,
                               end_date: end_date).csv }

    it "generates headers for the columns we need" do
      expect(subject.lines[0]).to eq("project,employee,date,duration,notes\n")
    end

    it "has one line for each entry (plus one for the headers)" do
      expect(subject.lines.count).to eq(5)
    end
  end

end
