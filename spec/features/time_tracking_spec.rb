require "rails_helper"

RSpec.feature "Tracking time" do
  let(:user) { FactoryBot.create :user }
  let!(:project) { FactoryBot.create :project }
  let!(:today) { Date.today }
  let!(:record_date) { Date.yesterday }

  scenario "when not logged in" do
    visit "/timers"
    expect(page.html).to have_text("Log in")
  end

  scenario "adding time with a duration", js: true do 
    login_as user

    visit "/timers"
    duration = "3.3"
    notes = "Some quality notes"

    select project.name, from: "entry_project_id"
    fill_in "entry_duration", with: duration
    fill_in "entry_notes", with: notes

    click_on 'Record time'

    within(".project") do
      expect(page).to have_content(notes)
      expect(page).to have_content(duration)
    end
  end

  scenario "shows a total of time tracked for the day", js: true do
    login_as user

    visit "/timers"

    select project.name, from: "entry_project_id"
    fill_in "entry_duration", with: "3"
    click_on "Record time"

    # if we sleep for some amount here, the test will pass
    # this works in a browser

    select project.name, from: "entry_project_id"
    fill_in "entry_duration", with: "2.5"
    click_on "Record time"

    # maybe this selector should be more specific
    expect(page).to have_content "5.5 hours"
  end

  scenario "takes you to the day you were on", js: true do
    login_as user
    visit "/timers?date=#{record_date.strftime("%Y-%m-%d")}"
    duration = "3"
    notes = "Some quality notes"

    select project.name, from: "entry_project_id"
    fill_in "entry_duration", with: duration
    fill_in "entry_notes", with: notes

    click_on 'Record time'

    expect(page.html).to have_text(record_date.strftime("%B %-d, %Y"))
  end

  scenario "redirects to the current date when given an invalid date" do
    login_as user
    visit "/timers?date=BOGUS"

    expect(page.html).to have_text(today.strftime("%B %-d, %Y"))
  end

  scenario "displays error messages if required fields are missing", js: true do
    login_as user

    visit "/timers"
    click_on "Start timer"

    within("#enter_time") do
      expect(page.html).to have_text("can't be blank")
    end
  end

  scenario "updates submit button text after failed submission", js: true do
    login_as user

    visit "/timers"
    click_on "Start timer"

    fill_in "entry_duration", with: "2.5"

    expect(page).to have_button("Record time")
  end
end
