require "rails_helper"

RSpec.feature "Navgation forwards and backwards in time" do
  let(:today) { Date.today.strftime("%B %-d, %Y") }
  let(:yesterday) { Date.yesterday.strftime("%B %-d, %Y") }
  let(:user) { FactoryBot.create :user }

  before do
    login_as user
  end

  scenario "for yesterday" do
    visit "/timers"

    click_link 'Yesterday'
    expect(page.html).to have_text(yesterday)
  end

  scenario "for the current day" do
    visit "/timers"
    expect(page.html).to have_text(today)
  end

  scenario "can easily navigate back to today" do
    visit "/timers"

    click_link 'Today'
    expect(page.html).to have_text(today)
  end
end