require "rails_helper"

RSpec.feature "A Fancy UI", js: true do
  before(:each) do
    login_as FactoryBot.create(:user)
    visit "/timers"
  end

  scenario "The submit button says 'start timer' if no duration is specified" do
    fill_in "entry_duration", with: ''
    within("#enter_time") do
      expect(page).to have_button 'Start timer'
    end
  end

  scenario "The submit button says 'record time' if duration is specified" do
    fill_in "entry_duration", with: '2.5'
    within("#enter_time") do
      expect(page).to have_button 'Record time'
    end
  end

  context "Displaying the logged-in user's name", js: false do
    let(:user_with_name)    { FactoryBot.create(:user, name: "Joe Friday") }
    let(:user_with_no_name) { FactoryBot.create(:user, name: nil) }

    scenario "when the user's name is set, displays name" do
      login_as user_with_name

      visit "/"

      expect(page).to have_content(user_with_name.name)
    end

    scenario "when the user's name has not been set, displays email" do
      login_as user_with_no_name

      visit "/"

      expect(page).to have_content(user_with_no_name.email)
    end
  end
end
