FactoryBot.define do
  factory :user do
    sequence :email do |n|
      "user#{n}@example.com"
    end

    password { 'dorpdorp' }
    password_confirmation { 'dorpdorp' }

    factory :admin_user do
      admin { true }
    end
  end
end
