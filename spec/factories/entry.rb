FactoryBot.define do
  factory :entry do
    user
    project
    duration { 3 }
    recorded_date { '2020-04-20' }

    factory :running_timer do
      duration { nil }
      start_time { Date.today.midnight }
    end

    factory :stopped_timer do
      duration { 3 }
      start_time { Date.today.midnight }
    end
  end
end